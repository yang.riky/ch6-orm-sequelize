var express = require('express');
var router = express.Router();
const { Address } = require('../models');

router.post('/', function (req, res, next) {
  Address.create({
    userId: req.headers.userid,
    recipient: req.body.recipient,
    phoneNumber: req.body.phoneNumber,
    label: req.body.label,
    address: req.body.address
  }).then(address => {
    res.status(200).json(address);
  }).catch(err => {
    res.status(500).json(err);
  });
});

/* GET addresses listing. */
router.get('/', function (req, res, next) {
  Address.findAll().then(addresses => {
    res.status(200).json(addresses);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.get('/:id', function (req, res, next) {
  Address.findOne({
    where: {
      id: req.params.id
    }
  }).then(address => {
    res.status(200).json(address);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.put('/:id', function (req, res, next) {
  Address.update({
    userId: req.headers.userid,
    recipient: req.body.recipient,
    phoneNumber: req.body.phoneNumber,
    label: req.body.label,
    address: req.body.address
  }, {
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Update address succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.delete('/:id', function (req, res, next) {
  Address.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Delete address succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

module.exports = router;
