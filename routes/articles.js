var express = require('express');
var router = express.Router();
const { Article, ArticleTag, Comment } = require('../models');

router.post('/', function (req, res, next) {
  Article.create({
    title: req.body.title,
    body: req.body.body,
    userId: req.headers.userid
  }).then(article => {
    res.status(200).json(article);
  }).catch(err => {
    res.status(500).json(err);
  });
});

/* GET articles listing. */
router.get('/', function (req, res, next) {
  Article.findAll({
    include: [ArticleTag, Comment]
  }).then(articles => {
    res.status(200).json(articles);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.get('/:id', function (req, res, next) {
  Article.findOne({
    where: {
      id: req.params.id
    },
    include: [ArticleTag, Comment]
  }).then(article => {
    res.status(200).json(article);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.put('/:id', function (req, res, next) {
  Article.update({
    title: req.body.title,
    body: req.body.body,
    userId: req.headers.userid
  }, {
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Update article succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.delete('/:id', function (req, res, next) {
  Article.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Delete article succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

module.exports = router;
