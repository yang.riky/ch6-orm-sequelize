var express = require('express');
var router = express.Router();
const { User, Address } = require('../models');

router.post('/', function (req, res, next) {
  User.create({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  }).then(user => {
    res.status(200).json(user);
  }).catch(err => {
    res.status(500).json(err);
  });
});

/* GET users listing. */
router.get('/', function (req, res, next) {
  User.findAll({
    include: Address
  }).then(users => {
    res.status(200).json(users);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.get('/:id', function (req, res, next) {
  User.findOne({
    where: {
      id: req.params.id
    },
    include: Address
  }).then(user => {
    res.status(200).json(user);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.put('/:id', function (req, res, next) {
  User.update({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  }, {
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Update user succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.delete('/:id', function (req, res, next) {
  User.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Delete user succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

module.exports = router;
