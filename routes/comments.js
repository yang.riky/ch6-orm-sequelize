var express = require('express');
var router = express.Router();
const { Comment } = require('../models');

router.post('/', function (req, res, next) {
  Comment.create({
    comment: req.body.comment,
    articleId: req.body.articleId,
    userId: req.headers.userid
  }).then(comment => {
    res.status(200).json(comment);
  }).catch(err => {
    res.status(500).json(err);
  });
});

/* GET comments listing. */
router.get('/', function (req, res, next) {
  Comment.findAll().then(comments => {
    res.status(200).json(comments);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.get('/:id', function (req, res, next) {
  Comment.findOne({
    where: {
      id: req.params.id
    }
  }).then(comment => {
    res.status(200).json(comment);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.put('/:id', function (req, res, next) {
  Comment.update({
    comment: req.body.comment,
    articleId: req.body.articleId,
    userId: req.headers.userid
  }, {
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Update comment succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.delete('/:id', function (req, res, next) {
  Comment.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Delete comment succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

module.exports = router;
