var express = require('express');
var router = express.Router();
const { ArticleTag } = require('../models');

router.post('/', function (req, res, next) {
  ArticleTag.create({
    articleId: req.body.articleId,
    tagId: req.body.tagId
  }).then(articleTag => {
    res.status(200).json(articleTag);
  }).catch(err => {
    res.status(500).json(err);
  });
});

/* GET articleTags listing. */
router.get('/', function (req, res, next) {
  ArticleTag.findAll().then(articleTags => {
    res.status(200).json(articleTags);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.get('/:id', function (req, res, next) {
  ArticleTag.findOne({
    where: {
      id: req.params.id
    }
  }).then(articleTag => {
    res.status(200).json(articleTag);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.put('/:id', function (req, res, next) {
  ArticleTag.update({
    articleId: req.body.articleId,
    tagId: req.body.tagId
  }, {
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Update article tag succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.delete('/:id', function (req, res, next) {
  ArticleTag.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Delete article tag succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

module.exports = router;
