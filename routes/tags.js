var express = require('express');
var router = express.Router();
const { Tag } = require('../models');

router.post('/', function (req, res, next) {
  Tag.create({
    tag: req.body.tag,
    description: req.body.description
  }).then(tag => {
    res.status(200).json(tag);
  }).catch(err => {
    res.status(500).json(err);
  });
});

/* GET tags listing. */
router.get('/', function (req, res, next) {
  Tag.findAll().then(tags => {
    res.status(200).json(tags);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.get('/:id', function (req, res, next) {
  Tag.findOne({
    where: {
      id: req.params.id
    }
  }).then(tag => {
    res.status(200).json(tag);
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.put('/:id', function (req, res, next) {
  Tag.update({
    tag: req.body.tag,
    description: req.body.description
  }, {
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Update tag succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

router.delete('/:id', function (req, res, next) {
  Tag.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(200).json({
      message: 'Delete tag succeed'
    });
  }).catch(err => {
    res.status(500).json(err);
  });
});

module.exports = router;
