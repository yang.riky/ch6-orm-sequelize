'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ArticleTag extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.ArticleTag.belongsTo(models.Article);
      models.ArticleTag.belongsTo(models.Tag);
    }
  }
  ArticleTag.init({
    articleId: DataTypes.INTEGER,
    tagId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'ArticleTag',
    tableName: 'article_tags',
    underscored: true,
    paranoid: true
  });
  return ArticleTag;
};