'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Address extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Address.belongsTo(models.User);
    }
  }
  Address.init({
    userId: DataTypes.INTEGER,
    recipient: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    label: DataTypes.STRING,
    address: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Address',
    tableName: 'addresses',
    underscored: true,
    paranoid: true
  });
  return Address;
};