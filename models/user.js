'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User.hasMany(models.Article, {
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      });
      models.User.hasMany(models.Comment, {
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      });
      models.User.hasMany(models.Address, {
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      });
    }
  }
  User.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'users',
    underscored: true,
    paranoid: true
  });
  return User;
};