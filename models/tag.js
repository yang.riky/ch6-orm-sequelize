'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Tag extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Tag.hasMany(models.ArticleTag, {
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      });
    }
  }
  Tag.init({
    tag: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Tag',
    tableName: 'tags',
    underscored: true,
    paranoid: true
  });
  return Tag;
};