var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tagsRouter = require('./routes/tags');
var addressesRouter = require('./routes/addresses');
var articlesRouter = require('./routes/articles');
var articleTagsRouter = require('./routes/articleTags');
var commentsRouter = require('./routes/comments');

var app = express();

const URL_API = '/api/v1/';

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use(URL_API + 'users', usersRouter);
app.use(URL_API + 'tags', tagsRouter);
app.use(URL_API + 'addresses', addressesRouter);
app.use(URL_API + 'articles', articlesRouter);
app.use(URL_API + 'articleTags', articleTagsRouter);
app.use(URL_API + 'comments', commentsRouter);

module.exports = app;
